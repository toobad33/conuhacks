//
//  ViewController.swift
//  Best Resto
//
//  Created by Charles Jiang on 2019-01-27.
//  Copyright © 2019 Charles Jiang. All rights reserved.
//

import UIKit
import CoreLocation

class ViewController: UIViewController, CLLocationManagerDelegate
{
    var text: String = "Hello World!"
    let locationManager = CLLocationManager()

    @IBOutlet weak var get_started: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.get_started.setTitle("Click here to get started", for: UIControl.State.normal)
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled()
        {
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
            
        }
    }

    @IBAction func get_started_fct(_ sender: UIButton)
    {
        locationManager.requestLocation()
        let alertController = UIAlertController(title: "Welcome!", message: "\(text)", preferredStyle: UIAlertController.Style.alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        present(alertController, animated: true, completion: nil)
        
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        if let lat = locations.last?.coordinate.latitude, let long = locations.last?.coordinate.longitude {
            text = "\(lat)" + ", " + "\(long)"
            print("\(lat), \(long)")
        }
        else{
            print("No coordinates, error")
        }

    }
    func locationManager(_ manager:CLLocationManager,didFailWithError error: Error) {
        print(error)
    }
    
}


